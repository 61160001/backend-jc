const JobPosting = require('../model/Jobposting')
const jobPostingsController = {
  async addJobPosting (req, res, next) {
    const payload = req.body
    payload.company = '602fbe6eceddfb124692ae39'
    const jobPosting = new JobPosting(payload)
    try {
      await jobPosting.save()
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJobPosting (req, res, next) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    try {
      /*
    Temp
    */
      payload.company = '602fbe6eceddfb124692ae39'
      const jobPosting = await JobPosting.updateOne(
        { _id: payload._id },
        payload
      )
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async delJobPosting (req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const jobPosting = await JobPosting.deleteOne({ _id: id })
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobPostings (req, res, next) {
    try {
      const jobPosting = await JobPosting.find({})
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobPosting (req, res, next) {
    const { id } = req.params
    try {
      const jobPosting = await JobPosting.findById(id)
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = jobPostingsController
