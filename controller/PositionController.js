const Position = require('../model/Position')
const positionController = {
  async getPositions (req, res, next) {
    console.log('test')
    Position.find({})
      .then(function (position) {
        res.json(position)
      })
      .catch(function (err) {
        res.status(500).send(err)
      })
  },
  async getPosition (req, res, next) {
    const { id } = req.params
    Position.findById(id)
      .then(function (position) {
        console.log(position)
        res.json(position)
      })
      .catch(function (err) {
        res.status(500).status(err)
      })
  },
  async addPosition (req, res, next) {
    const payload = req.body
    console.log(payload)
    const position = new Position(payload)
    try {
      position.save()
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updatePosition (req, res, next) {
    const payload = req.body
    try {
      console.log(payload)
      const position = await Position.updateOne({ _id: payload._id }, payload)
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deletePosition (req, res, next) {
    const { id } = req.params
    try {
      const position = await Position.deleteOne({ _id: id })
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = positionController
