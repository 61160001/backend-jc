const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('601eb76d1e72782ca8ad6178'),
    username: 'k0001',
    password: 'k0001',
    firstName: 'ขะจรพง',
    lastName: 'หัวดอ',
    birthday: '10/10/2542',
    email: 'khajonpong@huador.com',
    phoneNo: '0955040720',
    state: 'person',
    profile: 'gog001.pdf'
  }
]
