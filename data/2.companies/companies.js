const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('602fbe6eceddfb124692ae39'),
    compName: 'Burapha',
    logo: 'burapha.png',
    workPlace: '121/32 Phahonyothin Bangkok 10400',
    compType: 'industry',
    compSize: 'Big',
    compDescription: 'The best of web developer group',
    facebook: 'Burapha99',
    ig: 'Burapha99',
    line: '@Burapha99'
  },
  {
    _id: new ObjectID('60587ff502bbff5a13d5b1da'),
    compName: 'Mahachai',
    logo: 'maha.png',
    workPlace: '121/32 Saentung Trat 23150',
    compType: 'industry',
    compSize: 'Big',
    compDescription: 'The best of web developer group',
    facebook: 'Mahachai11',
    ig: 'maha',
    line: '@mahachai'
  }
]
