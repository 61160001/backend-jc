const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('602fbfda93f51e92200b6ee9'),
    name: 'Website Developer',
  },
  {
    _id: new ObjectID('6033b7db5b062490c308944e'),
    name: 'Backend Developer',
  }
]