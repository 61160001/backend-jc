const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('602fe36a92e124475c0036a8'),
    company: new ObjectID('602fbe6eceddfb124692ae39'),
    gender: 'male',
    age: 25,
    nationality: 'ไม่จำกัด',
    education: 'ไม่ต้องการประสบการณ์',
    worrkPosition: 'พนักงานขายหน้าร้าน',
    jobDescription: 'พนักงานคิดเงินเเละ จัดสินค้า',
    workPlace: '121/32 Saentung Trat 23150',
    salaryMin: 15000,
    salaryMax: 25000,
    contact: 'line:@mahachai'
  }
]
