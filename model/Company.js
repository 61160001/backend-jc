const mongoose = require('mongoose')
const Schema = mongoose.Schema
const comSchema = new Schema({
  compName: String,
  logo: String,
  workPlace: String,
  compType: String,
  compSize: String,
  compDescription: Boolean,
  facebook: String,
  ig: String,
  line: String
})

module.exports = mongoose.model('Company', comSchema)
