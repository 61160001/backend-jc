const mongoose = require('mongoose')
const Schema = mongoose.Schema
const jobPostingSchema = new Schema({
  company: {
    type: Schema.Types.ObjectId,
    ref: 'Company'
  },
  gender: String,
  age: Number,
  nationality: String,
  education: String,
  worrkPosition: String,
  jobDescription: String,
  workPlace: String,
  salaryMin: Number,
  salaryMax: Number,
  contact: String
})

module.exports = mongoose.model('Jobposting', jobPostingSchema)
