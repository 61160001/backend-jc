const mongoose = require('mongoose')

const PositionSchema = mongoose.Schema({
  jobName: String,
  jobDescript: String,
  position: String,
  salarymin: Number,
  salarymax: Number,
  gender: String,
  age: Number
})

module.exports = mongoose.model('Position', PositionSchema)
