const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  username: String,
  password: String,
  firstName: String,
  lastName: String,
  birthday: String,
  email: String,
  phoneNo: String,
  state: String,
  profile: String
})

module.exports = mongoose.model('User', userSchema)
