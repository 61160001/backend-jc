const express = require('express')
const router = express.Router()
const jobPostingsController = require('../controller/JobPostingController')

/* GET users listing. */
router.get('/', jobPostingsController.getJobPostings)

router.get('/:id', jobPostingsController.getJobPosting)

router.post('/', jobPostingsController.addJobPosting)

router.put('/', jobPostingsController.updateJobPosting)

router.delete('/:id', jobPostingsController.delJobPosting)

module.exports = router
