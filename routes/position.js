const express = require('express')
const router = express.Router()
const positionController = require('../controller/PositionController')

router.get('/', positionController.getPositions)

router.get('/:id', positionController.getPosition)

router.post('/', positionController.addPosition)

router.put('/', positionController.updatePosition)

router.delete('/:id', positionController.deletePosition)

module.exports = router
